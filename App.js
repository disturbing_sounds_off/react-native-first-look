import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, Alert} from 'react-native';

export default function App() {

    const [count, setCount] = useState(0);

    return (
        <View style={styles.container}>
            <Text>test text</Text>
            <Text style={styles.greating} >Hello Kolya</Text>

            <Button
                title="жми меня"
                onPress={() => {
                    Alert.alert('ты согрешил')
                    setCount(count + 1)
                }}
            />
            <Text style={styles.greating} >count грехов: {count}</Text>
            <StatusBar style="auto" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },


    greating: {
        fontSize: 32,
        color: "white",
        padding: 64
    }
});
